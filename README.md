## OpenProvider Test

## Reqs:
- Apache 2.4
- PHP >= 7.1
- MySQL 5.7

## Setup
### Create DB and set up config
Change credentials in:
```
src/settings.php
```

### Install deps
Run:
```
composer install
```

### Create tables structure
Run:
```
php src\cli.php /doctrine orm:schema-tool:create
```

### Import required data
Run:
```
php src\cli.php /doctrine dbal:import migration/settings.sql
```

### Add script to cron
``` 
01 * * * * php /PATH/TO/PROJECT/src/cli.php /congratulateWithBirthday
```

## DONE!

## Now you can work with API!
### Add new profile
``` 
curl -X POST \
  http://openprovider.loc/profiles \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{
	"kek": "cheburek",
	"firstName": "Thomas",
	"lastName": "Anderson",
	"position": "Software Engineer",
	"phone": "+78002000600",
	"email": "kek@cheburek.com",
	"birthday": "2012-06-25",
	"photo": "fff.jpg"
}'
```

### View profile
``` 
curl -X GET \
  http://openprovider.loc/profiles/1 \
  -H 'cache-control: no-cache'
}'
```

### View all profiles
``` 
curl -X GET \
  http://openprovider.loc/profiles \
  -H 'cache-control: no-cache'
}'
```

### Patch profile
``` 
curl -X PATCH \
  http://openprovider.loc/profiles/1 \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{
	"firstName": "John"
}'
```

### Patch settings
``` 
curl -X PATCH \
  http://openprovider.loc/settings/birthday_notifications_hours_before \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{
	"value": 5
}'
```