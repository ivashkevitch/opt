<?php

require __DIR__ . '/../vendor/autoload.php';

session_start();

// Instantiate the app
$settings = require __DIR__ . '/../src/settings.php';

$app = new \Slim\App($settings);

$c = $app->getContainer();
$c['notFoundHandler'] = function ($c) {
    return new \OpenProvider\Api\Exceptions\ApiExceptionsHandler();
};
$c['errorHandler'] = function ($c) {
    return new \OpenProvider\Api\Exceptions\ApiExceptionsHandler();
};

// Set up dependencies
require __DIR__ . '/../src/dependencies.php';

// Register middleware
require __DIR__ . '/../src/middleware.php';

// Register routes
require __DIR__ . '/../src/routes.php';

// Run app
$app->run();
