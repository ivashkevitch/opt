<?php

namespace OpenProvider\Domain\Infrastrucure\ORM;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\CachedReader;
use Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use OpenProvider\Domain\Infrastrucure\ORM\Types\BirthdayType;
use Slim\Container;

class EntityManagerFactory
{
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function createEntityManager(): EntityManager
    {
        \Doctrine\Common\Annotations\AnnotationRegistry::registerFile(
            __DIR__ . '/../../../../../vendor/doctrine/orm/lib/Doctrine/ORM/Mapping/Driver/DoctrineAnnotations.php'
        );

        $cache = new \Doctrine\Common\Cache\ArrayCache();
        $annotationReader = new AnnotationReader();
        $cachedAnnotationReader = new CachedReader($annotationReader, $cache);

        $driverChain = new MappingDriverChain();
        $annotationDriver = new AnnotationDriver($cachedAnnotationReader, $this->container['settings']['doctrine']['paths']);
        $driverChain->addDriver($annotationDriver, 'OpenProvider\Domain');

        $config = $this->initConfig($driverChain, $cache);
        $this->registerTypes();

        return EntityManager::create($this->container['settings']['doctrine']['connection'], $config);
    }

    /**
     * @param $driverChain
     * @param $cache
     * @return Configuration
     */
    private function initConfig($driverChain, $cache): Configuration
    {
        $config = new Configuration();
        $config->setProxyDir(sys_get_temp_dir());
        $config->setProxyNamespace('Proxy');
        $config->setAutoGenerateProxyClasses($this->container['settings']['doctrine']['isDev']);
        $config->setMetadataDriverImpl($driverChain);
        $config->setMetadataCacheImpl($cache);
        $config->setQueryCacheImpl($cache);
        $config->addCustomDatetimeFunction('dayofyear', \DoctrineExtensions\Query\Mysql\DayOfYear::class);
        return $config;
    }

    private function registerTypes()
    {
        if (!\Doctrine\DBAL\Types\Type::hasType(BirthdayType::BIRTHDAY_TYPE)) {
            \Doctrine\DBAL\Types\Type::addType(BirthdayType::BIRTHDAY_TYPE, BirthdayType::class);
        }
    }
}