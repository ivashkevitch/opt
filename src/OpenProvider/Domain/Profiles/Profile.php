<?php

namespace OpenProvider\Domain\Profiles;

use Doctrine\ORM\Mapping as ORM;
use OpenProvider\Domain\Core\Exceptions\InvalidArgumentException;

/**
 * @ORM\Entity()
 * @ORM\Table(name="profiles")
 **/
class Profile
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $firstName;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $lastName;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $position;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $phone;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $email;

    /**
     * @ORM\Column(type="birthday")
     * @var \DateTime
     */
    private $birthday;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $photo;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getPosition(): string
    {
        return $this->position;
    }

    /**
     * @param string $position
     */
    public function setPosition(string $position): void
    {
        $this->position = $position;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $rawEmail
     * @throws InvalidArgumentException
     */
    public function setEmail(string $rawEmail): void
    {
        $emailFiltered = filter_var($rawEmail, FILTER_VALIDATE_EMAIL);

        if ($emailFiltered === false) {
            throw new InvalidArgumentException(sprintf(
                'Email "%s" is not valid!',
                $rawEmail
            ));
        }

        $this->email = $emailFiltered;
    }

    /**
     * @return \DateTime
     */
    public function getBirthday(): \DateTime
    {
        return $this->birthday;
    }

    /**
     * @param \DateTime $birthday
     */
    public function setBirthday(\DateTime $birthday): void
    {
        $this->birthday = $birthday;
    }

    /**
     * @return string
     */
    public function getPhoto(): string
    {
        return $this->photo;
    }

    /**
     * @param string $photo
     */
    public function setPhoto(string $photo): void
    {
        $this->photo = $photo;
    }

    public function ensureIsValid()
    {
        if (!is_string($this->firstName) || $this->firstName === '') {
            throw new InvalidArgumentException('firstName must be not empty string');
        }
        if (!is_string($this->lastName) || $this->lastName === '') {
            throw new InvalidArgumentException('lastName must be not empty string');
        }
        if (!is_string($this->position) || $this->position === '') {
            throw new InvalidArgumentException('position must be not empty string');
        }
        if (!is_string($this->phone) || $this->phone === '') {
            throw new InvalidArgumentException('phone must be not empty string');
        }
        if (!is_string($this->email) || $this->email === '') {
            throw new InvalidArgumentException('email must be not empty string');
        }
        if (!$this->birthday instanceof \DateTime) {
            throw new InvalidArgumentException('birthday must be instance of DateTime');
        }
        if (!is_string($this->photo) || $this->photo === '') {
            throw new InvalidArgumentException('photo must be not empty string');
        }
    }
}
