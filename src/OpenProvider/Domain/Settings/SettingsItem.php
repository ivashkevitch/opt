<?php

namespace OpenProvider\Domain\Settings;

use Doctrine\ORM\Mapping as ORM;
use OpenProvider\Domain\Core\Exceptions\InvalidArgumentException;

/**
 * @ORM\Entity()
 * @ORM\Table(name="settings")
 **/
class SettingsItem
{
    public const BIRTHDAY_NOTIFICATIONS_HOURS_BEFORE = 'birthday_notifications_hours_before';

    public const AVAILABLE_PARAMS = [
        self::BIRTHDAY_NOTIFICATIONS_HOURS_BEFORE => 'int'
    ];

    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(type="json")
     * @var mixed
     */
    private $value;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @throws InvalidArgumentException
     */
    public function setName(string $name): void
    {
        $this->ensureIsCorrectName($name);
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     * @throws InvalidArgumentException
     */
    public function setValue($value): void
    {
        $this->ensureIsCorrectValue($value);
        $this->value = $value;
    }

    /**
     * @throws InvalidArgumentException
     */
    public function ensureIsValid()
    {
        $this->ensureIsCorrectName($this->name);
        $this->ensureIsCorrectValue($this->value);
    }

    /**
     * @param string $name
     * @throws InvalidArgumentException
     */
    private function ensureIsCorrectName(string $name): void
    {
        if (!array_key_exists($name, self::AVAILABLE_PARAMS)) {
            throw new InvalidArgumentException(sprintf(
                'It is unavailable parameter name in settings: "%s"',
                $name
            ));
        }
    }

    /**
     * @param $value
     * @throws InvalidArgumentException
     */
    private function ensureIsCorrectValue($value): void
    {
        $correctType = self::AVAILABLE_PARAMS[$this->name];

        if (is_scalar($value) && function_exists('is_' . $correctType)) {
            $functionName = 'is_' . $correctType;
            if ($functionName($value)) {
                return;
            }
        }

        if (!$value instanceof $correctType) {
            throw new InvalidArgumentException(sprintf(
                'Value of parameter "%s" must be with type "%s"',
                $this->name,
                $correctType
            ));
        }
    }
}
