<?php

namespace OpenProvider\Api\Controllers;

use Slim\Container;

abstract class AbstractApiController
{
    protected const DEFAULT_LIMIT = 10;
    protected const MAX_LIMIT = 100;

    /** @var Container */
    protected $container;

    protected $limit;

    protected $offset;

    public function __construct(Container $container)
    {
        $this->container = $container;

        $limit = filter_input(INPUT_GET, 'limit', FILTER_VALIDATE_INT) ?? static::DEFAULT_LIMIT;
        if ($limit > static::MAX_LIMIT) {
            $limit = static::MAX_LIMIT;
        }
        $this->limit = $limit;

        $this->offset = filter_input(INPUT_GET, 'offset', FILTER_VALIDATE_INT) ?? 0;
    }
}
