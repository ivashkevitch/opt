<?php

namespace OpenProvider\Api\Controllers;

use Doctrine\ORM\EntityManager;
use OpenProvider\Api\Exceptions\ApiBadRequestException;
use OpenProvider\Api\Exceptions\ApiNotFoundException;
use OpenProvider\Domain\Core\Exceptions\InvalidArgumentException;
use OpenProvider\Domain\Profiles\Profile;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;
use Symfony\Component\Serializer\Serializer;

class ProfileController extends AbstractApiController
{
    /** @var EntityManager */
    private $em;

    /** @var Serializer */
    private $serializer;

    public function __construct(Container $c)
    {
        parent::__construct($c);
        $this->em = $c->get('entityManager');
        $this->serializer = $c->get('serializer');
    }

    public function create(Request $request, Response $response, array $args)
    {
        try {
            /** @var Profile $profile */
            $profile = $this->serializer->deserialize($request->getBody()->getContents(), Profile::class, 'json');
            $profile->ensureIsValid();
        } catch (InvalidArgumentException $e) {
            throw new ApiBadRequestException($e->getMessage());
        }

        $this->em->persist($profile);
        $this->em->flush();

        return $response->withJson([
            'metadata' => [
                'count' => 1,
                'limit' => 0,
                'offset' => 0
            ],
            'profiles' => [json_decode($this->serializer->serialize($profile, 'json'))]
        ]);
    }

    public function find(Request $request, Response $response, array $args)
    {
        $qb = $this->em->createQueryBuilder();
        $profiles = $qb
            ->select('p')
            ->from(Profile::class, 'p')
            ->orderBy('p.id', 'desc')
            ->setMaxResults($this->limit)
            ->setFirstResult($this->offset)
            ->getQuery()
            ->getResult();

        $totalCount = (int) $this->em->createQueryBuilder()
            ->select('count(p)')
            ->from(Profile::class, 'p')
            ->getQuery()
            ->getSingleScalarResult();

        $profilesAsArray = array_map(function (Profile $profile) {
            return json_decode($this->serializer->serialize($profile, 'json'));
        }, $profiles);

        return $response->withJson([
            'metadata' => [
                'count' => $totalCount,
                'limit' => $this->limit,
                'offset' => $this->offset
            ],
            'profiles' => $profilesAsArray
        ]);
    }

    public function view(Request $request, Response $response, array $args)
    {
        $profile = $this->em->find(Profile::class, $args['id']);
        if ($profile === null) {
            throw new ApiNotFoundException('Profile not found');
        }

        return $response->withJson([
            'metadata' => [
                'count' => 1,
                'limit' => 0,
                'offset' => 0
            ],
            'profiles' => [json_decode($this->serializer->serialize($profile, 'json'))]
        ]);
    }

    public function patch(Request $request, Response $response, array $args)
    {
        $profile = $this->em->find(Profile::class, $args['id']);
        if ($profile === null) {
            throw new ApiNotFoundException('Profile not found');
        }

        try {
            /** @var Profile $profile */
            $profile = $this->serializer->deserialize(
                $request->getBody()->getContents(),
                Profile::class,
                'json',
                ['object_to_populate' => $profile]
            );
            $profile->ensureIsValid();
        } catch (InvalidArgumentException $e) {
            throw new ApiBadRequestException($e->getMessage());
        }

        $this->em->flush();

        return $response->withJson([
            'metadata' => [
                'count' => 1,
                'limit' => 0,
                'offset' => 0
            ],
            'profiles' => [json_decode($this->serializer->serialize($profile, 'json'))]
        ]);
    }
}