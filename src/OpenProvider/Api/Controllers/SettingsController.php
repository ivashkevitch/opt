<?php

namespace OpenProvider\Api\Controllers;

use Doctrine\ORM\EntityManager;
use OpenProvider\Api\Exceptions\ApiBadRequestException;
use OpenProvider\Api\Exceptions\ApiNotFoundException;
use OpenProvider\Domain\Core\Exceptions\InvalidArgumentException;
use OpenProvider\Domain\Profiles\Profile;
use OpenProvider\Domain\Settings\SettingsItem;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;
use Symfony\Component\Serializer\Serializer;

class SettingsController extends AbstractApiController
{
    /** @var EntityManager */
    private $em;

    /** @var Serializer */
    private $serializer;

    public function __construct(Container $c)
    {
        parent::__construct($c);
        $this->em = $c->get('entityManager');
        $this->serializer = $c->get('serializer');
    }

    public function find(Request $request, Response $response, array $args)
    {
        $qb = $this->em->createQueryBuilder();
        $settings = $qb
            ->select('s')
            ->from(SettingsItem::class, 's')
            ->getQuery()
            ->getResult();

        $totalCount = (int) $this->em->createQueryBuilder()
            ->select('count(s)')
            ->from(SettingsItem::class, 's')
            ->getQuery()
            ->getSingleScalarResult();

        $settingsAsArray = array_map(function (SettingsItem $settingsItem) {
            return json_decode($this->serializer->serialize($settingsItem, 'json'));
        }, $settings);

        return $response->withJson([
            'metadata' => [
                'count' => $totalCount,
                'limit' => 0,
                'offset' => 0
            ],
            'settings' => $settingsAsArray
        ]);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     * @throws ApiBadRequestException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     * @throws ApiNotFoundException
     */
    public function patch(Request $request, Response $response, array $args)
    {
        /** @var SettingsItem $settingsItem */
        $settingsItem = $this->em->find(SettingsItem::class, $args['name']);
        if ($settingsItem === null) {
            throw new ApiNotFoundException(sprintf(
                'Settings parameter with name "%s" not found',
                $args['name']
            ));
        }

        try {
            /** @var SettingsItem $settingsItem */
            $settingsItem = $this->serializer->deserialize(
                $request->getBody()->getContents(),
                SettingsItem::class,
                'json',
                ['object_to_populate' => $settingsItem]
            );
            $settingsItem->ensureIsValid();
        } catch (InvalidArgumentException $e) {
            throw new ApiBadRequestException($e->getMessage());
        }

        $this->em->persist($settingsItem);
        $this->em->flush();

        return $response->withJson([
            'metadata' => [
                'count' => 1,
                'limit' => 0,
                'offset' => 0
            ],
            'settings' => [json_decode($this->serializer->serialize($settingsItem, 'json'))]
        ]);
    }
}
