<?php

namespace OpenProvider\Api\Exceptions;

use Slim\Http\Request;
use Slim\Http\Response;

class ApiExceptionsHandler
{
    public function __invoke(Request $request, Response $response, \Throwable $exception = null) {
        switch (true) {
            case $exception === null:
                return $response
                    ->withStatus(404)
                    ->withJson(['error' => ['code' => 404, 'text' => 'Route not found!']]);
            case $exception instanceof ApiNotFoundException:
                return $response
                    ->withStatus(404)
                    ->withJson(['error' => ['code' => 404, 'text' => $exception->getMessage()]]);
            case $exception instanceof ApiBadRequestException:
                return $response
                    ->withStatus(400)
                    ->withJson(['error' => ['code' => 400, 'text' => $exception->getMessage()]]);
            default:
                return $response
                    ->withStatus(500)
                    ->withJson(['error' => [
                        'code' => 500,
                        'text' => 'Something went wrong!'
                    ]]);
        }
    }
}