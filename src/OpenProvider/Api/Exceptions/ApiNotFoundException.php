<?php

namespace OpenProvider\Api\Exceptions;

class ApiNotFoundException extends \Exception
{
}