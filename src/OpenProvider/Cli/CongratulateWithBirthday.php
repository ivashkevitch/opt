<?php

namespace OpenProvider\Cli;

use Doctrine\ORM\EntityManager;
use OpenProvider\Domain\Profiles\Profile;
use OpenProvider\Domain\Settings\SettingsItem;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;

class CongratulateWithBirthday
{
    /** @var EntityManager */
    private $em;

    public function __construct(Container $c)
    {
        $this->em = $c->get('entityManager');
    }

    public function run(Request $request, Response $response, array $args)
    {
        /** @var SettingsItem $settingsItem */
        $settingsItem = $this->em->find(SettingsItem::class, SettingsItem::BIRTHDAY_NOTIFICATIONS_HOURS_BEFORE);

        $hoursBeforeDayWithBirthday = $settingsItem->getValue();
        $dateTimeOfBirthday = new \DateTime(sprintf(
            'now + %d hours',
            $hoursBeforeDayWithBirthday
        ));
        $dateTimeOfBirthdayPlus1Hour = (clone $dateTimeOfBirthday)->modify('+1 hour');

        $qb = $this->em->createQueryBuilder();
        $profiles = $qb
            ->select('p')
            ->from(Profile::class, 'p')
            ->where('dayofyear(p.birthday) BETWEEN dayofyear(:startDateTime) AND dayofyear(:endDateTime)')
            ->setParameter('startDateTime', $dateTimeOfBirthday->format('Y-m-d'))
            ->setParameter('endDateTime', $dateTimeOfBirthdayPlus1Hour->format('Y-m-d'))
            ->getQuery()
            ->getResult();

        foreach ($profiles as $profile) {
            $this->sendEmail($profile);
        }

        return $response->withJson('done!');
    }

    private function sendEmail(Profile $profile)
    {
        // send email to user :)
        echo $profile->getEmail();
    }
}
