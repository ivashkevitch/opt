<?php

namespace OpenProvider\Cli;

use Doctrine\ORM\EntityManager;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;

class DoctrineController
{
    /** @var EntityManager */
    private $em;

    public function __construct(Container $c)
    {
        $this->em = $c->get('entityManager');
    }

    public function run(Request $request, Response $response, array $args)
    {
        try {
            $_SERVER['argv'] = $args;
            $helperSet = \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($this->em);
            \Doctrine\ORM\Tools\Console\ConsoleRunner::run($helperSet);
        } catch (\Throwable $e) {
            return $response->withJson($e->getMessage());
        }

        return $response->withJson($args);
    }
}