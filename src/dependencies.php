<?php
// DIC configuration

$container = $app->getContainer();

$container['serializer'] = function ($c) {
    $encoders = [new \Symfony\Component\Serializer\Encoder\JsonEncoder()];
    $normalizers = [
        new \Symfony\Component\Serializer\Normalizer\DateTimeNormalizer(DATE_ISO8601),
        new \Symfony\Component\Serializer\Normalizer\ObjectNormalizer(
            null,
            null,
            null,
            new Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor()
        ),
    ];
    return new \Symfony\Component\Serializer\Serializer($normalizers, $encoders);
};

$container['entityManager'] = function ($c) {
    $factory = new \OpenProvider\Domain\Infrastrucure\ORM\EntityManagerFactory($c);
    return $factory->createEntityManager();
};

