<?php

$app->get('/doctrine', function ($request, $response) use ($container, $argv) {
    return (new \OpenProvider\Cli\DoctrineController($container))
        ->run($request, $response, $argv);
});

$app->get('/congratulateWithBirthday', function ($request, $response) use ($container, $argv) {
    return (new \OpenProvider\Cli\CongratulateWithBirthday($container))
        ->run($request, $response, $argv);
});
