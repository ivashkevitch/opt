<?php

// Routes
$app->post('/profiles', \OpenProvider\Api\Controllers\ProfileController::class . ':create');
$app->get('/profiles', \OpenProvider\Api\Controllers\ProfileController::class . ':find');
$app->get('/profiles/{id}', \OpenProvider\Api\Controllers\ProfileController::class . ':view');
$app->patch('/profiles/{id}', \OpenProvider\Api\Controllers\ProfileController::class . ':patch');

$app->get('/settings', \OpenProvider\Api\Controllers\SettingsController::class . ':find');
$app->patch('/settings/{name}', \OpenProvider\Api\Controllers\SettingsController::class . ':patch');
