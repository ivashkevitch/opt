<?php
return [
    'settings' => [
        'displayErrorDetails' => true,
        'addContentLengthHeader' => false,

        'doctrine' => [
            'connection' => [
                'dbname' => 'openprovider',
                'user' => 'root',
                'password' => '',
                'host' => 'localhost',
                'driver' => 'pdo_mysql',
                'charset' => 'UTF8'
            ],
            'paths' => [
                __DIR__ . '/OpenProvider/Domain',
            ],
            'isDev' => true
        ],
    ],
];
