<?php

require __DIR__ . '/../vendor/autoload.php';

$settings = require __DIR__ . '/settings.php';

$argv = $GLOBALS['argv'];
array_shift($argv);

$env = \Slim\Http\Environment::mock(['REQUEST_URI' => reset($argv)]);

$settings['environment'] = $env;

$app = new \Slim\App($settings);

require __DIR__ . '/dependencies.php';
require __DIR__ . '/cli_routes.php';

$app->run();