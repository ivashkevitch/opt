SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

INSERT INTO `settings` (`name`, `value`) VALUES
('birthday_notifications_hours_before', '5');

COMMIT;